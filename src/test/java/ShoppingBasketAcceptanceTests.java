import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShoppingBasketAcceptanceTests {
    @Test
    public void Basket_With_Items_Has_Total_Price(){
        UserID userID = new UserID();
        ShoppingBasketService shoppingBasketService = new ShoppingBasketService();
        ProductCatalog productCatalog = new ProductCatalog();

        Product theHobbit = productCatalog.getProductByID(new ProductID(10002));
        Product breakingBad = productCatalog.getProductByID(new ProductID(20110));

        shoppingBasketService.addItem(userID, theHobbit.getProductID(), 2);
        shoppingBasketService.addItem(userID, breakingBad.getProductID(), 5);

        ShoppingBasket shoppingBasket = shoppingBasketService.basketFor(userID);

        assertEquals(45.00, shoppingBasket.getTotalPrice());
    }

}
